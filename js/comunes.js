/* JavaScript Document
   Autor: Enrique Brán
   Web: www.ia2web.com
   Todos los derechos reservados 2015 a CENGICAÑA
   */
$(function() {
  $( "#txLote" ).autocomplete({
    source: "autosearcher.php?obj=lote",
    minLength: 4,
    select: function( event, ui ) {
      $("#loteid").val(ui.item.id);
    }
  });
  $( "#txVariedad" ).autocomplete({
    source: "autosearcher.php?obj=var",
    minLength: 2,
    select: function( event, ui ) {
      $("#txVariedad").val(ui.item.id);
    }
  });
});

sumaFecha = function(d, fecha)
{
 var Fecha = new Date();
 fecha=fecha.replace(/-/g,"/");
 //alert(fecha);
 var sFecha = fecha || (Fecha.getDate() + "/" + (Fecha.getMonth() +1) + "/" + Fecha.getFullYear());
 var sep = sFecha.indexOf('/') != -1 ? '/' : '-'; 
 var aFecha = sFecha.split(sep);
 var fecha = aFecha[2]+'/'+aFecha[1]+'/'+aFecha[0];
 fecha= new Date(fecha);
 fecha.setDate(fecha.getDate()+parseInt(d));
 var anno=fecha.getFullYear();
 var mes= fecha.getMonth()+1;
 var dia= fecha.getDate();
 mes = (mes < 10) ? ("0" + mes) : mes;
 dia = (dia < 10) ? ("0" + dia) : dia;
 var fechaFinal = dia+sep+mes+sep+anno;
 return (fechaFinal);
 }