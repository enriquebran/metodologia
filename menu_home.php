<div id="menu_home" class="ui-widget-header ui-corner-all" style="width:650px;">
<table class="ui-pg-table">
	<tr>
		<th colspan="2"><h2>Ingreso de Datos</h2></th>
	</tr>
	<tr class="ui-widget-content jqgrow ui-row-ltr">
		<th>Componente</th>
		<th>Descripción</th>
	</tr>
	<tr>
		<td class="ui-state-default">
			<a href="?page=cana" id="acana">Caña</a>
		</td>
		<td>
			<p>*Se registra toda la información referente al cultivo.</p>
		</td>
	</tr>
	<tr class="ui-widget-content jqgrow ui-row-ltr">
		<td class="ui-state-default">
			<a href="?page=suelo" id="asuelo">Suelo</a>
		</td>
		<td>
			<p>*Descripción del suelo, características fisicas.</p>
		</td>
	</tr>
	<tr>
		<td class="ui-state-default">
			<a href="?page=clima" id="aclima">Clima</a>
		</td>
		<td>
			<p>Registro de información de variables meteorológicas.</p>
		</td>
	</tr>
	<tr class="ui-widget-content jqgrow ui-row-ltr">
		<td class="ui-state-default">
			<a href="?page=lluvia" id="alluvia">Lluvia-Diaria</a>
		</td>
		<td>
			<p>Seleccionar este componente en caso de no tener variables meteorologicas(TºC, HR%, Rg y V.V) diarias.</p>
		</td>
	</tr>
	<tr>
		<td class="ui-state-default">
			<a href="?page=riego" id="ariego">Riego (O. Actual)</a>
		</td>
		<td>
			<p>Funcionamiento del sistema y programación.</p>
		</td>
	</tr>
</table>
</div>
<br />
<div id="menu_home_resultados" class="ui-widget-header ui-corner-all" style="width:650px;">
	<table>
		<tr>
			<th colspan="2"><h2>Resultados</h2></th>
		</tr>
		<tr class="ui-widget-content jqgrow ui-row-ltr">
			<th>Componente</th>
			<th>Descripción</th>
		</tr>
		<tr class="ui-widget-content jqgrow ui-row-ltr">
			<td class="ui-state-default">
				<a href="?page=evapotranspiracion" id="evapotranspiracion">Evapotranspiración</a>
			</td>
			<td>
				<p>Estimación de la evapotranspiración potencial con base a la ecuación de PENMAN MONTEITH.</p>
			</td>
		</tr>
		<tr>
			<td class="ui-state-default">
				<a href="?page=balance_energetico" id="balance_energetico">Balance Energético</a>
			</td>
			<td>
				<p>Comportamiento de la radiación solar respecto en la superficie terrestre y su comparación con la energía que llega al límite de la atmosfera, Latitud 14º.</p>
			</td>
		</tr>
		<tr class="ui-widget-content jqgrow ui-row-ltr">
			<td class="ui-state-default">
				<a href="?page=bh_actual" id="bh_actual">BH - Manejo Actual del Riego</a>
			</td>
			<td rowspan="2">
				<p>Monitoreo del comportamiento de la humedad en el suelo.</p>
			</td>
		</tr>
		<tr>
			<td class="ui-state-default">
				<a href="?page=bh_simulado" id="bh_simulado">BH - Simulado</a>
			</td>
		</tr>
</table>
</div>