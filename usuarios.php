<?php 
	require_once("connect.php");
	$elMenuID = "2";
	include("revisarPermisos.php");
	include_once(LIB_ABS_PATH . "classes/class-catalogo.php"); 
	$catalogo = new catalogo($db,"Autorización de Usuarios");
	$catalogo->txTabla = "usuarios";
	if ((int)$_SESSION["CUsuarioID"]<>1) $catalogo->txWhere = "usuarioid<>1";
	$catalogo->setID('usuarioid',true);
	$catalogo->agregarCampo('Nombre','nombre');
	$catalogo->agregarCampo('Email/Usuario','email');
	$catalogo->agregarCampo('Tipo','tipo','enum');
	$catalogo->setCustomEdit('index.php?page=usuarios_edit','Usuarios','blank');
  	include_once(LIB_ABS_PATH . "includes.php");
	$catalogo->setAddEditDelete(true,true,true);
 	$catalogo->render(); 
?>