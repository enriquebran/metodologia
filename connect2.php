<?php
	require_once(dirname(__FILE__) . "/config.php");
 	session_start(SESSION_NAME);
	$_SESSION["HOME_PATH"] = HOME_PATH;
	//echo(HOME_PATH);
 	date_default_timezone_set('America/Guatemala');
	
	require_once(LIB_ABS_PATH . "classes/class-db.php");
	$db = new db_class();
	
	function ifZeroNull($aValue)
	{
		if(($aValue=='0')||($aValue=='')) return 'NULL';
		else return '"' . AddSlashes($aValue) . '"';
	}
	
	function AntiHack2($texto)
	{
		//$texto=str_replace(",","&#44;",$texto);
		$texto=str_replace("UNION","",$texto);
		$texto=str_replace("#","",$texto);
		$texto=str_replace("'","&lsquo;",$texto);
		$texto=str_replace('"','&quot;',$texto);
		$texto=str_replace('/','&frasl;',$texto);
		//$texto=utf8_decode($texto);
		return $texto;
	}

	function dateToMysql($aDateUS) {
		return substr($aDateUS,6,4) . "-" . substr($aDateUS,0,3) . substr($aDateUS,3,2);
	}
	
	$meses[1] = "Ene";
	$meses[2] = "Feb";
	$meses[3] = "Mar";
	$meses[4] = "Abr";
	$meses[5] = "May";
	$meses[6] = "Jun";
	$meses[7] = "Jul";
	$meses[8] = "Ago";
	$meses[9] = "Sep";
	$meses[10] = "Oct";
	$meses[11] = "Nov";
	$meses[12] = "Dic";
	$meses[01] = "Ene";
	$meses[02] = "Feb";
	$meses[03] = "Mar";
	$meses[04] = "Abr";
	$meses[05] = "May";
	$meses[06] = "Jun";
	$meses[07] = "Jul";
	$meses[08] = "Ago";
	$meses[09] = "Sep";
?>