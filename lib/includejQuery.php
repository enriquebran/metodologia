<?php
		if (!defined('JQUERY')) define('JQUERY','1.8.0');
		if (!defined('JQUERYUI'))	define('JQUERYUI','1.8.17');
?>

<?php if(JQUERYUI<>'') { 
	$tema = "css/jquery-ui-" . JQUERYUI . ".custom.css";
	//echo HOME_ABS_PATH . $tema;
	if (file_exists(HOME_ABS_PATH . $tema)) {
?>
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo(HOME_PATH . $tema); ?>"/>
<?php 
  } 
	else {
?>
	<link rel="stylesheet" type="text/css" media="screen" href="//code.jquery.com/ui/<?php echo(JQUERYUI); ?>/themes/base/jquery-ui.css" />
<?php
	}
}
?>
<script src="//ajax.googleapis.com/ajax/libs/jquery/<?php echo JQUERY ?>/jquery.min.js"></script>
<?php if(JQUERYUI<>'') { ?>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/<?php echo JQUERYUI ?>/jquery-ui.min.js"></script>
<?php } ?>

<script type="text/javascript">
  window.jQuery || document.write('<script type="text/javascript" src="<?php echo LIB_PATH ?>js/jquery-<?php echo JQUERY ?>.min.js"><\/script>');
<?php if(JQUERYUI<>'') { ?>
  window.jQuery || document.write('<script type="text/javascript" src="<?php echo LIB_PATH ?>js/jquery-ui-<?php echo JQUERYUI ?>.custom.min.js"><\/script>');
<?php } ?>
</script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-76684735-1', 'auto');
  ga('send', 'pageview');

</script>