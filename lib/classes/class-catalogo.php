<?php
/**********************************************************************************
ver 1.2
Clase Catalogo
Compuservice

function agregarBoton($aTitulo, $aAccion, $aEnviarID, $aIcon)
Para agregar botones adicionales a la barra inferior.
Recibe los parametros titulo, accion, enviarid, icon
- enviarid true/false indica si se le concatena a la accion el ID de la fila seleccionada
  ej:  $aAccion=clientes_contactos.php?id=   $aEnviarID=true => clientes_contactos.php?id=4
- icon es cualquier icono del jqueryUI (Se pueden ver en http://jqueryui.com/themeroller/

ver 1.1 => respetar Add/Edit/Delete si tien customEdit y Modal top al popup custom
ver 1.2 => Se arreglaron los warnings de PHP
***********************************************************************************/

require_once("class-pcrypt.php");

class catalogo {
	protected  
	  $database, $sizeDiv="";
	
  var
	$idName, $pageName,
	$numrows=200, $height="300px", 
	$titulos=array(), $campos=array(), $camposSinAlias=array(), $editable=array(), 
	$dataType=array(), $cmbQuery=array(), $realField=array(),
	$botones=array(), $align=array(), $customFooter=array(),
	$query="", $queryCampos="", $id="", $txTabla="",
	$header="", $decryptText="", 
	$customEdit="", $customEditTitle="", $customEditTarget="popup", $Title="", $txJoin="", $txGroupBy="", $txWhere="",
	$showID=false, $inlineEdit=false,
	$add=true, $edit=true, $delete=true;
			
  function __construct($db,$aHeader="", $asizeDiv="#info", $aIdName = "list4", $aPageName = "page4") {
            $this->database = $db;
			$this->header=$aHeader;
			$this->sizeDiv=$asizeDiv;
			$this->decryptText = new encrypt;
			$this->idName = $aIdName;			
			$this->pageName = $aPageName;
        }
				
	public function agregarCampo($aTitulo, $aCampo, $aForceType="", $aCmbQuery="", $aRealField="", $aEditable=true, $aAlign='left') {
		$this->titulos[]   = $aTitulo;
		$this->campos[]    = $aCampo;
		$this->forceType[] = $aForceType;
		$this->cmbQuery[]  = $aCmbQuery;
		$this->realField[] = $aRealField;
		$this->editable[]  = ($aEditable==true?"true":"false");
		$this->align[] = $aAlign;
	}
	
	public function setID($aID, $aShow=false) {
		$this->id = $aID;
		$this->showID  = ($aShow==false?"true":"false");
	}
	
	public function setCustomEdit($aFile, $aTitle, $aTarget="popup") {
		$this->customEdit = $aFile;
		$this->customEditTitle = ($aTitle);
		$this->customEditTarget = $aTarget;
	}
	
	public function setAddEditDelete($aAdd=true, $aEdit=true, $aDelete=true){
		$this->add=$aAdd;
		$this->edit=$aEdit;
		$this->delete=$aDelete;
	}

	public function agregarBoton($aTitulo, $aAccion, $aEnviarID=true, $aIcon='ui-icon-star',$aVentana="_blank") {
		$this->botones[] = array('titulo'=>$aTitulo, 'accion'=>$aAccion, 'enviarid'=>$aEnviarID, 'icon'=>$aIcon, 'ventana'=>$aVentana );
	}
	
	public function setFooter($aColNo, $aValor) {
		$this->customFooter[] =array('columna'=>$aColNo,'valor'=>$aValor);
	}
  
	public function armarQuery() {
		$elWhere="";
		$this->queryCampos = implode($this->campos,',');
		
		if ($this->txWhere <>'') $elWhere = " WHERE " . $this->txWhere;
		return "SELECT " . $this->id . "," . $this->queryCampos . " FROM " . $this->txTabla . " " . $this->txJoin . " " . $elWhere . " " . $this->txGroupBy;
	}
	
	public function render() {
		//El div que contiene la tabla y que se ajusta a las dimensiones que se manden de parametro
		echo("<table id='".$this->idName."'></table><div id='".$this->pageName."'></div>\n");
	
		$this->queryCampos = implode($this->campos,',');
		if ($this->txWhere <>'') $elWhere = " WHERE " . $this->txWhere;
		$this->query = $this->armarQuery();
		//echo($this->query);
		$dataSet = $this->database->execQuery($this->query);
		echo('<script language="javascript">
		  var lastsel3;
			$(document).ready(function() {	
				jQuery("#'.$this->idName.'").jqGrid({ 
					datatype: "json",
					colNames:[\'ID\',\'' . implode("','", $this->titulos) . '\'], 
					colModel:[{name:\'' . $this->id . '\',index:\'' . $this->id . '\', 
							  searchoptions: { sopt: [\'cn\', \'eq\', \'lt\', \'gt\']},
							  width:40, hidden:' . $this->showID . ', key:true},');
					$modelo = "";
					if($dataSet==true) 
					for($i=0;$i<$this->database->getNumFields($dataSet)-1;$i++)
							{		
								//Se le quita el alias al nombre del campo para la edicion solo si es un campo editable!
								if($this->editable[$i]=="true") {
								$pos = strpos($this->campos[$i],".");
								if($pos===false)
									$this->camposSinAlias[$i] = $this->campos[$i];	
								else
									$this->camposSinAlias[$i] = substr($this->campos[$i],$pos+1);	
								}
								else
									$this->camposSinAlias[$i] = $this->campos[$i];
								if($this->realField[$i]!="") $this->camposSinAlias[$i]=$this->realField[$i];
							
								//Aqui se guarda el arreglo de los tipos de datos de los campos, o si se forza a interpretar como otro dato (parametro en agregarcampo)
								$this->dataType[$i] = ($this->forceType[$i]!="")?$this->forceType[$i]:$this->database->getDataType($dataSet,$i+1) ;	
							
								$modelo .= "{align:'" . $this->align[$i] . "',name:'" . $this->camposSinAlias[$i]  . //"_".$this->dataType[$i].
									"', searchoptions: { sopt: ['cn', 'eq', 'lt', 'gt']}, index:'" . $this->campos[$i] . 
									"', editable: " . $this->editable[$i];
								//Ahora se agregan las opciones de edicion dependiendo del tipo de dato.	
								if(($this->dataType[$i]=="datetime")||($this->dataType[$i]=="timestamp")||($this->dataType[$i]=="date")) {
									$modelo .= ",editoptions:{size:15, maxlength:15, 
									dataInit: function(elem){setTimeout(function(){
									    jQuery('.ui-datepicker').css({'font-size':'75%'});	
									    $(elem).datepicker( {
									    		showButtonPanel: true,
												changeMonth: true,
												changeYear: true,
												dateFormat: 'yy-mm-dd'
											} );
											$(elem).datepicker($.datepicker.regional['es']);		
									 }, 200);}}";
								}
								else if($this->dataType[$i]=="blob") {
									$modelo .= ",edittype:\"textarea\", editoptions:{rows:\"10\", cols:\"70\"}";
								}
								else if($this->dataType[$i]=="enum") {
									$modelo .= ",edittype:\"select\", editoptions:{value:\"";
									$comboText = "";
									
									foreach($this->database->getEnum($this->txTabla, $this->camposSinAlias[$i]) as $value)
											$comboText .= $value . ":" . $value . ";";
											
									$comboText = substr($comboText,0,-1);
									$modelo .= $comboText;
									$modelo .= "\"}";
								}
								//Si trae un query incluido en el agregarcampo, se utiliza este query para crear el combobox
								if($this->cmbQuery[$i]!="")
								{
									$modelo .= ",edittype:\"select\" ,editoptions:{value:\"";
									$comboText = "";
									$combo2 = $this->database->execQuery($this->cmbQuery[$i]);
									//echo('Error:' . mysql_error());
									while($combo = $this->database->openQuery($combo2)) {
									  $comboText .= $combo[1] . ":" . $combo[0] . ";";
									}
									$comboText = substr($comboText,0,-1);
									$modelo .= $comboText;
									$modelo .= "\"}";
								}
								
								$modelo .=	($this->titulos[$i]==""?",hidden:true":"") . "},\n"; //Si el titulo viene vacio, esconder columna
							}		 
					$modelo = substr($modelo,0,-2);
					echo($modelo); 
					echo('
					],');
					
			if($this->inlineEdit==true)
			{
				
				echo("onSelectRow: function(id){ 
					if(id && id!==lastsel3){ 
						jQuery('#".$this->idName."').jqGrid('restoreRow',lastsel3); 
						jQuery('#".$this->idName."').jqGrid('editRow',id,true); 
						lastsel3=id; 
					}
					},");
			}
				
			$aCatalogo = json_encode($this);
	
			echo('rowNum:' . $this->numrows . ',
					pager: jQuery(\'#'.$this->pageName.'\'),
					viewrecords: true,									
					caption: "' . $this->header . '",');
			if(!empty($this->customFooter))
				echo("footerrow : true, userDataOnFooter : true,");
			
			$_SESSION["sExtras"] = $this->decryptText->encode(json_encode($this->database));
			$_SESSION["sCatalogo"] = $this->decryptText->encode($aCatalogo);
			echo('sortable: true,
					url:"' . LIB_PATH . 'classes/class-catalogo-getdata.php",
					editurl:"' . LIB_PATH . 'classes/class-catalogo-save.php"
					
				}).navGrid("#'.$this->pageName.'",{' . ($this->customEdit==""?'edit:' . ($this->edit?'true':'false') . ',add:' . ($this->add?'true':'false') . ',': 'edit:false,add:false,') . 'del:' . ($this->delete?'true':'false') . '},{},{},{},{multipleSearch:true})');
				//Si va a ser editado con una forma externa, aqui se crean los botones y el popup con la nueva forma.
				if($this->customEdit=="")
					echo(';');
				else
				 {
					 //Solo muestra los botones de edicion custom si se desea
					if($this->edit)
						echo('.jqGrid(\'navButtonAdd\',"#'.$this->pageName.'",{ caption:"", buttonicon:"ui-icon-pencil", onClickButton:function(){addEdit(\'edit\');}, position: "first", title:"Editar", cursor: "pointer"} )');
					if($this->add)
						echo('.jqGrid(\'navButtonAdd\',"#'.$this->pageName.'",{ caption:"", buttonicon:"ui-icon-plus", onClickButton:function(){addEdit(\'add\');}, position: "first", title:"Nuevo", cursor: "pointer"} );');
					
					echo("\nfunction addEdit(aOper) {
						
						if(aOper=='add') { " . 
							($this->customEditTarget=="popup"?"$('#popup').load('" . $this->customEdit . "?oper=' + aOper); ":"window.location='" . $this->customEdit . (strpos($this->customEdit,'?')==false?"?":"&") . "oper=' + aOper;") . "
							
						}
						else if (aOper=='edit') {
							var selr = jQuery('#".$this->idName."').jqGrid('getGridParam','selrow'); 
							if(selr) {
								" . ($this->customEditTarget=="popup"?"$('#popup').load('" . $this->customEdit . "?oper=' + aOper + '&id=' + selr);":"window.location='" . $this->customEdit . (strpos($this->customEdit,'?')==false?"?":"&") . "oper=' + aOper + '&id=' + selr;") . "
							}
							else {
								alert('Seleccione una fila'); 
							  return false;
							}	
						}
							");
					//El dialog solo lo muestra si el tipo de custom edit es popup, sino lo muestra en ventana nueva y no debe sacar este popup
					if($this->customEditTarget=="popup") echo("
							$('#popup').dialog({width:'auto', autoResize: true,
							  modal: true, 
								position: ['center','top'],
								title:'" . $this->header . "',
								buttons:{
									'Guardar': function(){
										$('form').submit();
									},
									'Cancelar':  function(){	$(this).dialog('close');
								  }
								},
								open: function() {
								$(this).parent().find('.ui-dialog-buttonpane button:contains(\"Cancelar\")').button({
										icons: { primary: 'ui-icon-close' }
								});
								$(this).parent().find('.ui-dialog-buttonpane button:contains(\"Guardar\")').button({
										icons: { primary: 'ui-icon ui-icon-disk' }
								});
								} //buttons
						});
");
echo("
				} //function addEdit; 
					");
					
				}			
				//INICIO Botones adicionales
				foreach($this->botones as $key=>$value) {
				echo('jQuery("#'.$this->idName.'").jqGrid(\'navButtonAdd\',"#'.$this->pageName.'",{ caption:"' . $value['titulo'] . '", buttonicon:"' . $value['icon'] . '", 
				  onClickButton:function(){
						var selr = jQuery(\'#'.$this->idName.'\').jqGrid(\'getGridParam\',\'selrow\');');
				if($value['enviarid']) {
					echo("if (selr==null) {alert('Debe seleccionar una fila'); return false;} 
					else ");
				}
				echo('	window.open(\'' . $value['accion'] . '\'' . ($value['enviarid']?'+selr':'')  . ',\''.$value['ventana'].'\', \'' . $value['titulo'] . '\');');
				echo('}, position: "first", title:"' . $value['titulo'] . '", cursor: "pointer"} );');
				}
				//print_r($this->botones);
				//FIN Botones adicionales	
				echo('jQuery("#'.$this->idName.'").jqGrid(\'filterToolbar\',{stringResult:true});

				jQuery(window).bind(\'resize\', function() {
    			jQuery("#'.$this->idName.'").setGridWidth(jQuery("' . $this->sizeDiv . '").width());
					jQuery("#'.$this->idName.'").setGridHeight(jQuery(window).height()-280);
				}).trigger(\'resize\');

				}); //del document.ready
				</script>');
  }
}
?>
<div id="popup" title=""></div>