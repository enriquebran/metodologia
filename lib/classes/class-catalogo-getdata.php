<?php
	session_start();
  require_once("class-pcrypt.php");
	$aDecrypt = new encrypt;
	$catalogo = json_decode($aDecrypt->decode($_SESSION["sCatalogo"]));
	$dataDB = json_decode($aDecrypt->decode($_SESSION["sExtras"]));

  define('DB_HOST',$dataDB->host);
  define('DB_USER',$dataDB->username);
  define('DB_PASS',$dataDB->password);
	define('DB_DB',$dataDB->db); 
	date_default_timezone_set('America/Guatemala');
  include ("class-db.php");
  $db = new db_class();
	//var_dump($_REQUEST);
	//var_dump($_GET["aCatalogo"]);
	//var_dump($catalogo);
	//echo("<br>");
	//exit;
	$page  = addslashes($_GET['page']); // get the requested page 
	$limit = addslashes($_GET['rows']); // get how many rows we want to have into the grid 
	$sidx  = addslashes($_GET['sidx']); // get index row - i.e. user click to sort 
	$sord  = addslashes($_GET['sord']); // get the direction 
	
	if(!$sidx) $sidx =1;  

	if($_REQUEST["_search"]=="true") {
		$searchParams = json_decode(stripslashes($_REQUEST["filters"]));
		$groupOperation = $searchParams->groupOp;
		
		$where = " WHERE " . ($catalogo->txWhere<>""?$catalogo->txWhere . " AND ":"") . "(";

		foreach($searchParams->rules as $value) {

			$op = $startTxt = $endTxt = '';
			switch($value->op){
				case "cn":
					$op = 'LIKE';
					$startTxt = $endTxt = '%';
				break;
				case "gt":
					$op = '>';
				break;
				case "lt":
					$op = '<';
				break;
				default: $op = '=';
			}

			$where .= $value->field . " $op '$startTxt" . $value->data . "$endTxt' " . $groupOperation . " ";

		}
		$where = substr($where, 0,-4);
		$where .= ")";
	}
	else
		if($catalogo->txWhere!="") 
			$where = " WHERE " . $catalogo->txWhere;
		else $where = "";
		
		
	$count = $db->openSingleQuery("SELECT COUNT(*) FROM " . $catalogo->txTabla . " " . $catalogo->txJoin . $where);
	$total_pages = ($count>0?ceil($count/$limit):0);
	if ($page > $total_pages) $page=$total_pages; 
	
	$start = $limit*$page - $limit;
	if($start<0) $start=0;
	
	$query = "SELECT " . $catalogo->id .  "," . $catalogo->queryCampos . " FROM " . $catalogo->txTabla . " " . 
		$catalogo->txJoin . " " . $where . " " . $catalogo->txGroupBy . " ORDER BY $sidx $sord LIMIT $start , $limit"; 
	//echo($query); exit;
	$result = $db->execQuery($query); 
	//echo(mysql_error());
	
	$response->page    = $page; 
	$response->total   = $total_pages; 
	$response->records = $count; 
	$i=0; 
	while($row = $db->openQuery($result, MYSQL_NUM)) { 
	   $response->rows[$i]['id']=$row[0]; //El id
		 foreach($row as $key=>$value) {	 
		 	 switch($catalogo->dataType[$key-1]) {
			 	case "datetime": 
					$valor = $db->fechaHumano($value);
					break;
				default: $valor = $value;
			 }
			 $response->rows[$i]['cell'][$key] = $valor;
		 }
		 $i++; 
		} 

	//Si hay custom footer, se setean las variables
	foreach($catalogo->customFooter as $key=>$value) {
		$temp = (array)$value;
		$response->userdata[$catalogo->camposSinAlias[$temp['columna']]] = $temp['valor'];
	}
	echo json_encode($response);
?>