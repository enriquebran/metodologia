<?php
//ver 1.0
//CREATE TABLE `menu`( `menuid` INT UNSIGNED NOT NULL AUTO_INCREMENT , `padreid` INT UNSIGNED , `nombre` VARCHAR(100) , `link` VARCHAR(100) , `target` ENUM('_blank','_self','_top') NOT NULL DEFAULT '_self' , `orden` INT NOT NULL DEFAULT '0' , `icono` VARCHAR(100) , PRIMARY KEY (`menuid`) ); 

class menu_class {
	protected $database, $tablaMenu, $contentDiv, $permisos;	
	
	function __construct($adb,$atablaMenu="menu", $acontentDiv="#info", $aPermisos="") {
				$this->database = $adb;
				$this->tablaMenu = $atablaMenu;
				$this->contentDiv = $acontentDiv;
				$this->permisos = $aPermisos;
		}
					
	public function render() {
		echo('<style type="text/css">
		      .topnav { height:28px;}
					.topnav ul, .topnav {padding:0;margin:0;}
					.topnav li { list-style:none; position:relative; float:left;
							padding: 0 8px 0 5px; }
					.topnav a { display: block; 
							 line-height: 25px; text-decoration: none; 
							 height:27px;
							 } 
					.level2 { position:absolute; top:25px; left:0; display:none; z-index:5000;} 	
					.level2 li {width: 100%;}
					.level3 { position:absolute; top:0px; left:50px; display:none; z-index:5000;} 	
					.level3 li {width: 100%;}
					.subNivel a {float: left;}
					.subNivel .mas {float:right; padding-top: 4px;}
					</style>');
		echo("<ul class='topnav ui-widget ui-widget-content'>");
		$maxLength=0;
		$maxLength3=0;
		$titant="\"--\"";
		if($this->permisos!="")
			$where = " AND menuid IN(" . $this->permisos . ")";
		else
			$where = "";
		$query = "SELECT menuid, nombre, link FROM " . $this->tablaMenu . " WHERE padreid IS NULL " . $where .  " ORDER BY orden";		
		$menu2 = $this->database->execQuery($query);
		echo(mysql_error());
		while($menu=$this->database->openQuery($menu2))
		{
			echo('<li class="ui-state-default"> <a href="' . $menu["link"] . '">' . $menu["nombre"] . '</a>');
			$submenu2 = $this->database->execQuery("SELECT nombre, link ,target, orden, menuid, icono FROM " . $this->tablaMenu . 
				" WHERE padreid=" . $menu["menuid"] . $where . " ORDER BY orden");
			$primero=true;
			while($submenu=$this->database->openQuery($submenu2)) {
				if($primero==true) { echo("\n<ul class='level2'>"); $primero=false; }
				echo('<li class="ui-state-default subNivel"><a href="?page=' . $submenu["link"] .'">' . $submenu["nombre"] . '</a><div class="mas">&nbsp;</div>');
				if($maxLength<strlen($submenu["nombre"])) $maxLength = strlen($submenu["nombre"]);
				//Nivel 3 
				$dsSub3 = $this->database->execQuery("SELECT nombre, link ,target, orden, menuid, icono FROM " . $this->tablaMenu . 
					" WHERE padreid=" . $submenu["menuid"] . $where . " ORDER BY orden");
				$primero3=true;
				while($submenu3=$this->database->openQuery($dsSub3)) {
					if($primero3==true) { echo("\n<ul class='level3'>"); $primero3=false; }
					echo('<li class="ui-state-default subNivel3"><a href="?page=' . $submenu3["link"] .'">' . $submenu3["nombre"] . '</a></li>');
					if($maxLength3<strlen($submenu3["nombre"])) $maxLength3 = strlen($submenu3["nombre"]);
				}
				if($primero3==false) echo("</ul>");
				echo("</li>");
			}
			if($primero==false) echo("</ul>");
			echo("</li>"); 	
		}
		echo("<li class='ui-state-default'><a href='" . LIB_PATH . "classes/class-loginoperar.php?act=logout'>Cerrar sesi&oacute;n</a></li></ul>");

echo('<script language="javascript">
(function($){$.fn.hoverIntent=function(f,g){var cfg={sensitivity:7,interval:100,timeout:0};cfg=$.extend(cfg,g?{over:f,out:g}:f);var cX,cY,pX,pY;var track=function(ev){cX=ev.pageX;cY=ev.pageY};var compare=function(ev,ob){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);if((Math.abs(pX-cX)+Math.abs(pY-cY))<cfg.sensitivity){$(ob).unbind("mousemove",track);ob.hoverIntent_s=1;return cfg.over.apply(ob,[ev])}else{pX=cX;pY=cY;ob.hoverIntent_t=setTimeout(function(){compare(ev,ob)},cfg.interval)}};var delay=function(ev,ob){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);ob.hoverIntent_s=0;return cfg.out.apply(ob,[ev])};var handleHover=function(e){var ev=jQuery.extend({},e);var ob=this;if(ob.hoverIntent_t){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t)}if(e.type=="mouseenter"){pX=ev.pageX;pY=ev.pageY;$(ob).bind("mousemove",track);if(ob.hoverIntent_s!=1){ob.hoverIntent_t=setTimeout(function(){compare(ev,ob)},cfg.interval)}}else{$(ob).unbind("mousemove",track);if(ob.hoverIntent_s==1){ob.hoverIntent_t=setTimeout(function(){delay(ev,ob)},cfg.timeout)}}};return this.bind(\'mouseenter\',handleHover).bind(\'mouseleave\',handleHover)}})(jQuery);
			$(document).ready(function(){  
			  $(".topnav .subNivel").width(' . $maxLength . '*6);
				$(".topnav .subNivel3").width(' . $maxLength3 . '*6);
				$(".topnav .level3").css("left","' . ($maxLength*6+12) . 'px");
				
				$(".topnav li").has("ul").children(".mas").html("&raquo;");
				
				$(".topnav li").hoverIntent({timeout:500});
				$(".topnav li").hoverIntent(function(){
					$(this).children("ul").show();
				}, function(){
					$(this).children("ul").hide();
				});

  			$(".topnav li").hover(
					function () {
						$(this).addClass("ui-state-hover");
					}, 
					function () {
						$(this).removeClass("ui-state-hover");
					}
				);
});
</script>'); 
	} //render
} //class
?>

