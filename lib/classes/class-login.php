<?php
//ver 1.0.2
//Changelog
//1.0.1 => Se arreglaron los notices de PHP
//1.0.2 => Se cambio el include por includejQuery.php (para no necesitar del grid)
//1.0.3 => Se arreglo para que redirija a la pagina original que hizo la llamada.   
          //El connect.php debe ir de esta forma:	
					//if ($_SESSION["CUsuarioID"]=="") {
					//header("Location: login.php?redirect=" . urlencode(( (isset($_SERVER['HTTPS'])&&($_SERVER['HTTPS']!='off'))   ?"https":"http") . 
					//	"://" . $_SERVER['SERVER_NAME'] . ":" . $_SERVER['SERVER_PORT'] .  $_SERVER['REQUEST_URI']));
					//exit;	  
					//}
//1.0.4 => Se agrego la funcion setAuthToken(), la cual da mayor seguridad,  el connect.php debe ir de esta forma:
					//	$elToken = $db->openSingleQuery("SELECT token FROM usuarios WHERE usuarioid=" . $_SESSION["CUsuarioID"]);
					//if ( (!isset($_SESSION["CLogged"])) || ($_SESSION["CToken"]<>$elToken)  ) {				
class login {
	var
	  $database, $logo="", $table="usuarios", $MD5=true,
		$userfield="email", $passwordfield="password", $tituloUsuario="Email", $tituloPassword="Contrase&ntilde;a", $tituloAcceder="Acceder",
		$campos=array(), $sessionVar=array(), $tokenField="";
		
	function __construct($db,$aLogo="", $aTable="usuarios", $aUserField="email", $aPasswordField="password", $aMD5=true) {
		$this->database = $db;
		$this->logo=$aLogo;
		$this->table=$aTable;
		$this->userfield=$aUserField;
		$this->passwordfield=$aPasswordField;
		$this->MD5=$aMD5;
	}
	
	function setAuthToken($aCampo) {
		$this->tokenField=$aCampo;
	}
	
	function setCamposExtras($aCampos, $aSessionVar) {
		$this->campos=$aCampos;
		$this->sessionVar=$aSessionVar;
	}
	
	function setTitulos($aUsuario="Email", $aPassword="Contrase&ntilde;a", $aAcceder="Acceder") {
	  $this->tituloUsuario=$aUsuario;
		$this->tituloPassword=$aPassword;
		$this->tituloAcceder=$aAcceder;
	}
	
	 public function render() {
		 include_once("class-pcrypt.php");
	   $aDecrypt = new encrypt;
		 $_SESSION["HOME_PATH"] = HOME_PATH;
		?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Login</title>
<meta content="width=device-width; height=device-height;" name="viewport">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<script language="javascript">
function processLogin() {
	str = $("form").serialize();

	$.get("<?php echo(LIB_PATH); ?>classes/class-loginoperar.php?extras=<?php echo($aDecrypt->encode(json_encode($this->database)) ); ?>&campos=<?php echo($aDecrypt->encode(json_encode($this->campos)) ); ?>&sessionvars=<?php echo($aDecrypt->encode(json_encode($this->sessionVar)) ); ?>&" + str, 
		function(data) { 
		  if (data!=1) {
				$("#login_error").html('<strong>Error: &nbsp;</strong>' + data); 
				$("#login_error").show();
				$("#loginform").effect("shake", { times:3 }, 50);
			} else {	
			  $('#LoginPrompt').dialog('close');
				window.location = "<?php echo(isset($_REQUEST["redirect"])?urldecode($_REQUEST["redirect"]):"index.php");  ?>"; 
			}
		});
	}
	
	$(document).ready(function () {	
	
		$("#btnSubmit").button().click(function(){
				processLogin();
		});
		
		$(".usuario").focus();		
	});
	
	
	$(document).bind('keydown', function(evt) {
		switch(evt.which) {
			case 13:	// Enter
				processLogin();
				break;
		}
	});
</script>

<style type="text/css">
 {
    margin: 0;
    padding: 0;
}

body {
    font-family: sans-serif;
    font-size: 12px;
    padding-top: 2px;
    
	background-color:#fff;
/*    background-image: url("chrome://global/skin/media/imagedoc-darknoise.png");*/
	background: url('images/WallpaperUMG.jpg') no-repeat;
	text-align:center;
}
#content{
	/*background: url("images/Rocio840x464.jpg") no-repeat;
	background: url('images/WallpaperUMG.jpg') no-repeat;*/
	height: 560px;
	margin: 0px auto;
	/*border-radius: 5px;
	border-color: #CC0000;
	box-shadow: 0 4px 7px -1px rgba(200, 200, 200, 0.7);*/
}

#login {
    margin: 1em auto;
    width: 320px;
	text-align:left;
	padding-top: 70px;
}
#login img {
	margin-left:30px;
	}
div.error, .login #login_error {
    background-color: #FFEBE8;
    border-color: #CC0000;
}

#login_error, .message {
    border-radius: 3px 3px 3px 3px;
    border-style: solid;
    border-width: 1px;
    margin: 0 0 16px 8px;
    padding: 5px;
	display:none;
}

form {
    background: none repeat scroll 0 0 #FFFFFF;
    border: 1px solid #E5E5E5;
    border-radius: 3px 3px 3px 3px;
    box-shadow: 0 4px 10px -1px rgba(200, 200, 200, 0.7);
    font-weight: normal;
    margin-left: 8px;
    padding: 26px 24px 46px;
}

label {
    color: #777777;
    font-size: 14px;
}

body form .input {
    background: none repeat scroll 0 0 #FBFBFB;
    border: 1px solid #E5E5E5;
    box-shadow: 1px 1px 2px rgba(200, 200, 200, 0.2) inset;
    font-family: "HelveticaNeue-Light","Helvetica Neue Light","Helvetica Neue",sans-serif;
    font-size: 24px;
    font-weight: 200;
    margin-bottom: 16px;
    margin-right: 6px;
    margin-top: 2px;
    outline: medium none;
    padding: 3px;
    width: 97%;
	color: #555555;
}

#login form p {
    margin-bottom: 0;
}

form .submit, .alignright {
    float: right;
		margin-right: 6px;
}

#login form p {
    margin-bottom: 0;
}

</style>
</head>
<body class="login">
	<div id="content">
	  <div id="login">
	    <img src="<?php echo($this->logo); ?>">
	    
	    <form name="loginform" id="loginform">
	      <p>
	        <label><?php echo($this->tituloUsuario); ?><br />
	        <input type="text" name="user|<?php echo($this->userfield);?>" id="user|<?php echo($this->userfield);?>" class="input usuario" tabindex="10" size="20"/></label>
	      </p>
	      <p>
	        <label><?php echo($this->tituloPassword); ?><br />
	        <input type="password" name="pass|<?php echo($this->passwordfield);?>" id="pass|<?php echo($this->passwordfield);?>" class="input" size="20" tabindex="20"/>
	        </label>
	      </p>
	      <p class="submit">
	        <input type="button" value="<?php echo($this->tituloAcceder); ?>" id="btnSubmit" tabindex="100" />
	        <input type="hidden" name="tabla" id="tabla" value="<?php echo($this->table);?>" />
	        <input type="hidden" name="md5" id="md5" value="<?php echo($this->MD5);?>" />
	        <input type="hidden" name="token" id="token" value="<?php echo($this->tokenField);?>" />
	      </p>
	    </form>
	    <div id="login_error"></div>
	  </div>
  	</div>
</body>
</html>
<?php 
	 }
}
?>