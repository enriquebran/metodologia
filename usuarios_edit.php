<?php  
  $elMenuID = "2";
	include("revisarPermisos.php");
  require_once("connect.php");
  $elID = (int)$_REQUEST["id"];
?>
<script src="<?php echo(HOME_PATH); ?>js/jquery.validate.min.js" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$("#guardar").button();
		$("#guardar").click(function(){
			$("#frmUsuarios").submit();
		});
			
		var validator = $("#frmUsuarios").validate({
		rules: {
			txNombre: {
				required:true
			},
			txEmail: {
			  required:true
				//email: true
			}
			<?php
				if($elID=="0") {
			?>
			,txPassword: {
			  required:true
			}
			<?php
				}
			?>
		},
		messages: {
			txNombre: "<div class='ui-widget'><div class='ui-state-error ui-corner-all' style='padding: 0pt 0.7em;'><p><span class='ui-icon ui-icon-alert' style='float: left; margin-right: 0.3em;'></span>El nombre es requerido</p></div></div>",
			txEmail: "<div class='ui-widget'><div class='ui-state-error ui-corner-all' style='padding: 0pt 0.7em;'><p><span class='ui-icon ui-icon-alert' style='float: left; margin-right: 0.3em;'></span>Ingrese una direcci&oacute;n de correo v&aacute;lida</p></div></div>"						
		},
		// the errorPlacement has to take the table layout into account
		errorPlacement: function(error, element) {
			if ( element.is(":radio") )
				error.appendTo( element.parent().next().next() );
			else if ( element.is(":checkbox") )
				error.appendTo ( element.next() );
			else
				error.appendTo( element.parent());
		},
		// set this class to error-labels to indicate valid fields
		success: function(label) {
			// set &nbsp; as text for IE
			label.html("&nbsp;").addClass("checked");
		}
	});
		
		$('.padre').click(function(){
				$(this).parents('fieldset:eq(0)').find(':checkbox').prop('checked', $(this).prop('checked'));
		});
	});
</script>
<?php
	if($elID<>0) {
	  echo("<h2>Edici&oacute;n de Usuario</h2>");
	  $datos = $db->fetchQuery("SELECT * FROM usuarios WHERE usuarioid=" . $elID);
	  //print_r($datos);
	}
	else{
	  echo("<h2>Nuevo usuario</h2>");
	  $datos["nombre"]="";
	  $datos["email"]="";
	  $datos["tipo"]="";
	}
?>
<form action="usuarios_operar.php" name="frmUsuarios" id="frmUsuarios" method="post">
<input type="hidden" name="id" value="<?php echo($elID); ?>" />
<table id="tblUsuarios2">
<tr>
<td colspan="3" valign="top">
  <tr>
    <td>Nombre:</td>
    <td><input type="text" name="txNombre" id="txNombre" value="<?php echo $datos["nombre"] ?>"></td>
  </tr>
  <tr>
    <td>Email/Usuario:</td>
    <td><input type="text" name="txEmail" id="txEmail" value="<?php echo $datos["email"] ?>"></td>
  </tr>
  <tr>
    <td>Contraseña:</td>
    <td><input type="password" name="txPassword" id="txPassword"></td>
  </tr>
  <tr>
    <td>Tipo:</td>
    <td><?php echo $db->comboFillEnum('usuarios','tipo',"","",$datos["tipo"]); ?></td>
  </tr>
</table>
<div id="divPermisos">
  <ul>
      <?php 
$menu2=$db->execQuery("SELECT m.nombre, m.orden, m.menuid,  
    (SELECT um.usuariomenuid FROM usuariomenus um WHERE 
      um.menuid=m.menuid AND um.usuarioid='" . $elID . "') AS permiso
  FROM menu m WHERE m.padreid IS NULL ORDER BY m.orden");
while($menu=$db->openQuery($menu2)) {
  echo("<li><fieldset><input type='checkbox' class='chkPermisos padre' name='chkPermisos[]' 
    id='chk" . $menu["menuid"] ."' value='" . $menu["menuid"] . "'" . 
    ($menu["permiso"]<>""?" checked='checked'":"") . " />" . $menu["nombre"]);
  //Primer submenu
  $submenu2=$db->execQuery("SELECT m.nombre, m.orden, m.menuid,   
    (SELECT um.usuariomenuid FROM usuariomenus um WHERE 
      um.menuid=m.menuid AND um.usuarioid='" . $elID . "') AS permiso
  FROM menu m WHERE m.padreid=" . $menu["menuid"] . " ORDER BY m.orden");
  $primero=true;
  while($submenu=$db->openQuery($submenu2)) {
    if($primero==true) { echo("<ul>"); $primero=false;}
    echo("<li><input type='checkbox' class='chkPermisos' name='chkPermisos[]' 
      id='chk" . $submenu["menuid"] ."' value='" . $submenu["menuid"] . "'" . 
      ($submenu["permiso"]<>""?" checked='checked'":"") . " />" . $submenu["nombre"]);
    //Segundo submenu	
    $subsubmenu2=$db->execQuery("SELECT m.nombre, m.orden, m.menuid,   
      (SELECT um.usuariomenuid FROM usuariomenus um WHERE 
        um.menuid=m.menuid AND um.usuarioid='" . $elID . "') AS permiso
    FROM menu m WHERE m.padreid=" . $submenu["menuid"] . " ORDER BY m.orden");
    $primerosub=true;
    while($subsubmenu=$db->openQuery($subsubmenu2)) {
      if($primerosub==true) { echo("<ul>"); $primerosub=false;}
      echo("<li><input type='checkbox' class='chkPermisos' name='chkPermisos[]'
        id='chk" . $subsubmenu["menuid"] ."' value='" . $subsubmenu["menuid"] . "'" . 
        ($subsubmenu["permiso"]<>""?" checked='checked'":"") . " />" . $subsubmenu["nombre"] . "</li>");
    }
    if($primerosub==false) echo("</ul>");
    echo("</li>");
  }
  if($primero==false) echo("</ul>");
  echo("</fieldset></li>");
}
?>
      </ul>
<div class="clear"></div>
<hr />
</div>
<div class="clear"></div>
<br />
<a id="guardar" href="javascript:void(0);">Guardar</a>
</form>