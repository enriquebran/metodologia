<?php
	require_once("connect.php");
	$elMenuID = "3";
	include("revisarPermisos.php");
	include_once(LIB_ABS_PATH . "classes/class-catalogo.php");
	$catalogo = new catalogo($db,"Menu");
	$catalogo->txTabla = "menu";
	$catalogo->setID('menuid',true);
	$catalogo->agregarCampo('Padre','padreid',"","SELECT nombre,menuid  FROM menu");
	$catalogo->agregarCampo('Nombre','nombre');
	$catalogo->agregarCampo('Link','link');
	$catalogo->agregarCampo('Target','target','enum');
	$catalogo->agregarCampo('Orden','orden');
	include_once(LIB_ABS_PATH . "includes.php");
	$catalogo->setAddEditDelete(true,true,true);
	$catalogo->render(); 
?>
