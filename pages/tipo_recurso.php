<?php
	require_once("connect.php");
	$elMenuID = "12";
	include("revisarPermisos.php");
	include_once(LIB_ABS_PATH . "classes/class-catalogo.php");
	$catalogo = new catalogo($db,"Tipo de Recursos");
	$catalogo->txTabla = "tbl_tipo_recurso";
	$catalogo->setID('idTipoRecurso',false);
	$catalogo->agregarCampo('Descripción','descripcion');
	include_once(LIB_ABS_PATH . "includes.php");
	$catalogo->setAddEditDelete(true,true,true);
	$catalogo->render(); 
?>