<?php
	$elUsuario=$_SESSION["CUsuarioID"];
	require_once("connect.php");
	$elMenuID = "14";
	require_once("revisarPermisos.php");
	$elID=(int)$_GET["id"];
	//echo $elID;
	include_once(LIB_ABS_PATH . "classes/class-catalogo.php");
	$catalogo = new catalogo($db,"Componentes");
	$catalogo->txTabla = "tbl_componente";
	$catalogo->txJoin = "INNER JOIN tbl_recurso r USING(idRecurso)
						 INNER JOIN usuarios ON (usuarios.usuarioid=tbl_componente.usuario_ingreso)
						 LEFT JOIN usuarios u ON (u.usuarioid=tbl_componente.usuario_modificacion)";
	$catalogo->setID('idComponente',false);
	if($elID<>0){
		$catalogo->txWhere="tbl_componente.idRecurso=".$elID;
		$catalogo->agregarCampo('ID Recurso (auto)','tbl_componente.idRecurso',"",'SELECT CONCAT(codigo," - ",nombre) as recurso, idRecurso FROM tbl_recurso WHERE idRecurso='.$elID);

	}
	else $catalogo->agregarCampo('ID Recurso','tbl_componente.idRecurso',"",'SELECT CONCAT(codigo," - ",nombre) as recurso, idRecurso FROM tbl_recurso');
	if($elID==0) $catalogo->agregarCampo('Descripción Recurso','r.nombre','','','',false);
	$catalogo->agregarCampo('Componente','tbl_componente.nombre');
	$catalogo->agregarCampo('Estado','tbl_componente.estado');
	$catalogo->agregarCampo('Fecha Ingreso','tbl_componente.fecha_ingreso','date');
	$catalogo->agregarCampo('ID Usuario Ingresó','tbl_componente.usuario_ingreso','',"SELECT nombre, usuarioid FROM usuarios",'',true);
	$catalogo->agregarCampo('Usuario Ingresó','usuarios.nombre','','','',false);
	$catalogo->agregarCampo('Modificado','tbl_componente.fecha_modificacion','','','',false);
	$catalogo->agregarCampo('ID Modificó (auto)','tbl_componente.usuario_modificacion','',"SELECT nombre, usuarioid FROM usuarios WHERE usuarioid=$elUsuario;",'',true);
	$catalogo->agregarCampo('Modificó','u.nombre','',"SELECT nombre, nombre FROM usuarios WHERE usuarioid=$elUsuario;",'',false);
	include_once(LIB_ABS_PATH . "includes.php");
	$catalogo->setAddEditDelete(true,true,true);
	$catalogo->render();
?>