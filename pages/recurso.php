<?php
	$elUsuario=$_SESSION["CUsuarioID"];
	require_once("connect.php");
	$elMenuID = "13";
	include("revisarPermisos.php");
	include_once(LIB_ABS_PATH . "classes/class-catalogo.php");
	$catalogo = new catalogo($db,"Recursos");
	$catalogo->txTabla = "tbl_recurso";
	$catalogo->txJoin = "LEFT JOIN tbl_tipo_recurso tr USING(idTipoRecurso)
						 INNER JOIN usuarios ON (usuarios.usuarioid=tbl_recurso.usuario_ingreso)
						 LEFT JOIN usuarios u ON (u.usuarioid=tbl_recurso.usuario_modificacion)";
	$catalogo->setID('idRecurso',TRUE);
	$catalogo->agregarCampo('ID Tipo','tbl_recurso.idTipoRecurso','','SELECT descripcion, idTipoRecurso FROM tbl_tipo_recurso;','',true);
	$catalogo->agregarCampo('Tipo','tr.descripcion','','','',false);
	$catalogo->agregarCampo('Código','tbl_recurso.codigo');
	$catalogo->agregarCampo('Nombre','tbl_recurso.nombre');
	$catalogo->agregarCampo('Estado','tbl_recurso.estado');
	$catalogo->agregarCampo('Fecha Ingreso','tbl_recurso.fecha_ingreso','date');
	$catalogo->agregarCampo('ID Usuario Ingresó','tbl_recurso.usuario_ingreso','',"SELECT nombre, usuarioid FROM usuarios",'',true);
	$catalogo->agregarCampo('Usuario Ingresó','usuarios.nombre','','','',false);
	$catalogo->agregarCampo('Modificado','tbl_recurso.fecha_modificacion','','','',false);
	$catalogo->agregarCampo('ID Modificó','tbl_recurso.usuario_modificacion','',"SELECT nombre, usuarioid FROM usuarios WHERE usuarioid=$elUsuario;",'',true);
	$catalogo->agregarCampo('Modificó','u.nombre','',"SELECT nombre, nombre FROM usuarios WHERE usuarioid=$elUsuario;",'',false);
	$catalogo->agregarBoton("Detalles", "?page=pages/detalle_recurso&id=",true,'ui-icon-star',"_blank");
	include_once(LIB_ABS_PATH . "includes.php");
	$catalogo->setAddEditDelete(true,true,true);
	$catalogo->render(); 
?>