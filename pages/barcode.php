<?php
	$elMenuID = "13";
    require_once("../connect.php");
    $elCodigo = $_REQUEST["code"];
?>
<?php
    require_once(LIB_ABS_PATH.'barcode/class/BCGFontFile.php');
    require_once(LIB_ABS_PATH.'barcode/class/BCGColor.php');
    require_once(LIB_ABS_PATH.'barcode/class/BCGDrawing.php');
    require_once(LIB_ABS_PATH.'barcode/class/BCGcode39.barcode.php');
    
    $font = new BCGFontFile(LIB_ABS_PATH.'barcode/font/Arial.ttf', 10);
    // The arguments are R, G, and B for color.
    $colorFront = new BCGColor(0, 0, 0);
    $colorBack = new BCGColor(255, 255, 255);

    // Barcode Part
    $code = new BCGcode39();
    $code->setScale(2);
    $code->setThickness(30);
    $code->setForegroundColor($colorFront);
    $code->setBackgroundColor($colorBack);
    $code->setFont($font);
    $code->setChecksum(false);

	if($elCodigo<>"0") {
     $code->parse($elCodigo);
   	}
	else{
    $code->parse('ERROR');
	}
  // Drawing Part
    $drawing = new BCGDrawing('', $colorBack);
    $drawing->setBarcode($code);
    $drawing->draw();
    header('Content-Type: image/png');

    $drawing->finish(BCGDrawing::IMG_FORMAT_PNG);
?>