<?php
	require_once("connect.php");
	$elMenuID = "8";
	include("revisarPermisos.php");
	include_once(LIB_ABS_PATH . "classes/class-catalogo.php");
	$catalogo = new catalogo($db,"Centros Universitarios");
	$catalogo->txTabla = "tbl_centro";
	$catalogo->setID('idCentro',false);
	$catalogo->agregarCampo('Nombre','nombre');
	$catalogo->agregarCampo('Director','nombre_director');
	$catalogo->agregarCampo('Estado','estado','enum');
	include_once(LIB_ABS_PATH . "includes.php");
	$catalogo->setAddEditDelete(true,true,true);
	$catalogo->render(); 
?>