<?php
	require_once("connect.php");
	$elMenuID = "9";
	include("revisarPermisos.php");
	include_once(LIB_ABS_PATH . "classes/class-catalogo.php");
	$catalogo = new catalogo($db,"Catedráticos");
	$catalogo->txTabla = "tbl_catedratico";
	$catalogo->txJoin = "LEFT JOIN tbl_centro ce USING(idCentro)";
	$catalogo->setID('idCatedratico',false);
	$catalogo->agregarCampo('Nombre','tbl_catedratico.nombre');
	$catalogo->agregarCampo('ID Centro','tbl_catedratico.idCentro','','SELECT nombre, idCentro FROM tbl_centro;','',true);
	$catalogo->agregarCampo('Centro Universitario','ce.nombre','','','',false);
	$catalogo->agregarCampo('Codigo','tbl_catedratico.codigo');
	$catalogo->agregarCampo('Teléfono','tbl_catedratico.telefono');
	$catalogo->agregarCampo('Correo','tbl_catedratico.correo');
	$catalogo->agregarCampo('Dirección','tbl_catedratico.direccion');
	$catalogo->agregarCampo('Estado','tbl_catedratico.estado','enum');
	include_once(LIB_ABS_PATH . "includes.php");
	$catalogo->setAddEditDelete(true,true,true);
	$catalogo->render(); 
?>