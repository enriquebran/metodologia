<?php
	require_once("connect.php");
	$elMenuID = "11";
	$elUsuario=$_SESSION["CUsuarioID"];
	require_once("revisarPermisos.php");
	include_once(LIB_ABS_PATH . "classes/class-catalogo.php");
	$catalogo = new catalogo($db,"Prestamos");
	$catalogo->txTabla = "tbl_prestamo";
	$catalogo->txJoin = "INNER JOIN usuarios u USING(usuarioid)
						 INNER JOIN tbl_catedratico c ON (c.idCatedratico=tbl_prestamo.idCatedratico)
						 INNER JOIN tbl_recurso r ON (r.idRecurso=tbl_prestamo.idRecurso)";
	$catalogo->setID('idPrestamo',false);
	$catalogo->agregarCampo('Centro','tbl_prestamo.idCentro',"","SELECT nombre, idCentro FROM tbl_centro");
	$catalogo->agregarCampo('ID Usuario (auto)','tbl_prestamo.usuarioid',"","SELECT nombre, usuarioid FROM usuarios WHERE usuarioid=".$elUsuario);
	$catalogo->agregarCampo('Usuario','u.nombre',"","","",false);
	$catalogo->agregarCampo('ID Catedrático','tbl_prestamo.idCatedratico',"","SELECT nombre, idCatedratico FROM tbl_catedratico");
	$catalogo->agregarCampo('Catedrático','c.nombre',"","","",false);
	$catalogo->agregarCampo('ID Recurso','tbl_prestamo.idRecurso',"","SELECT CONCAT(codigo,'-',nombre) recurso, idRecurso FROM tbl_recurso");
	$catalogo->agregarCampo('Recurso','r.nombre',"","","",false);
	$catalogo->agregarCampo('Fecha','tbl_prestamo.fecha','date');
	$catalogo->agregarCampo('Estado Recurso','tbl_prestamo.estado_recurso');
	$catalogo->agregarCampo('Estado Prestamo','tbl_prestamo.estado_prestamo');
	include_once(LIB_ABS_PATH . "includes.php");
	$catalogo->setAddEditDelete(true,true,true);
	$catalogo->render(); 
?>