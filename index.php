<?php
	require_once(dirname(__FILE__) . "/config.php");
	error_reporting(E_ALL);
	ini_set('display_errors','Off');
	require_once("connect.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php 
	include_once(LIB_ABS_PATH . "includes.php"); 
	include (LIB_ABS_PATH . "classes/class-menutop.php");
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta content="width=device-width" name="viewport">
<title>UMG</title>

<link rel="stylesheet" type="text/css" media="screen" href="<?php echo(HOME_PATH ); ?>css/base.css"/>
</head>
<body>
<div id="header">
	<div id="logo">
		<a href="<?php echo HOME_PATH;?>index.php"><img src="images/logoUMGtransparente.png" width="80" height="70" alt="Logo UMG" /></a>
	</div> <!--logo -->
	<h1 class="entry-title">UMG</h1>
	<div id="menu" class="">
		<?php
			$elMenu = new menu_class($db,"menu","#info", $_SESSION["CMenus"]);
			$elMenu->render(); 
		?>
	</div>
</div> <!--header--> 
  <div id="content" class="ui-corner-all">
    <div id="info"><br />
			<?php
				if(!isset($_GET["page"])) $_GET["page"]='home';
				$archivo = HOME_ABS_PATH . $_GET["page"] . ".php";
				//echo($archivo);
				if(file_exists($archivo))
					include(addslashes($_GET["page"]) . ".php"); 
				else
					echo "Elemento no encontrado.";
					?>
    </div>
  </div>
  <div id="footer" style="text-align:center;">Grupo 6 | 2016<?php if(date('Y')>2016) echo("-".date('Y')); ?> for UMG. |</div> <!--footer -->
<script type="text/javascript">
	$( "#radioset" ).buttonset();
	$(document).ready(function(){
		$("#btnBuscar").button();
		$("#admin").click(function(){
			alert("No disponible");
		});
		//$( "#menu" ).menu();
		$("#go").button();
		$( "#radioset" ).buttonset();
		$("#radioset").change(function(){
			if($("input[name='radio']:checked").val()=='home'){
				 document.location.href="index.php";
			 }
			 
		});
	});
</script>
</body>
</html>